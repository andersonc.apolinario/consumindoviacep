﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ConsumindoViaCep
{
    public class Endereco

    {
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Localidade { get; set; }
        public string Uf { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string EnderecoCompleto { get; set; }

    }


    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    public partial class MainPage : ContentPage
    {
        public Endereco Endereco { get; set; }

        public MainPage()
        {
            InitializeComponent();

            this.Endereco = new Endereco();

            this.BindingContext = this;

            BuscarEndereco();
        }


        public async void BuscarEndereco()
        {
            Console.WriteLine("Iniciando consumo da API");

            Uri url = new Uri("https://viacep.com.br/ws/"
                + this.Endereco.Cep + "/json/");

            // call endpoint
            HttpResponseMessage httpResponse =
                await Services.HttpService.GetRequest(url.AbsoluteUri);




            if (httpResponse.IsSuccessStatusCode)
            {
                string stringResponse = httpResponse
                    .Content.ReadAsStringAsync().Result;
                Console.WriteLine("\n====================");
                Console.WriteLine(stringResponse);

                Endereco End = Services.SerializationService
                    .DeserializeObject<Endereco>(stringResponse);

                //this.Endereco = End;
                //this.Endereco.EnderecoCompleto =
                //    End.Logradouro + ", "
                //    + End.Bairro + ", "
                //    + End.Localidade + " - "
                //    + End.Uf;
                if (End.Logradouro == "")
                {
                    this.Endereco.EnderecoCompleto = End.Localidade + " - " + End.Uf;

                }
                else

                {
                    this.Endereco.EnderecoCompleto =
                        End.Logradouro + ", "
                        + End.Bairro + ", "
                        + End.Localidade + " - "
                        + End.Uf;
                }

                OnPropertyChanged(nameof(this.Endereco));
            }

            ActivityIndicatorBuscar.IsRunning = false;

        }


        private void Button_Buscar_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine(this.Endereco.Cep);
            ActivityIndicatorBuscar.IsRunning = true;
            BuscarEndereco();
        }
    }
}
